# Tools List for the Course

Below tools are included in the `.rar` file

<br>

* Advanced_IP_Scanner_2.5.4594.1.exe

* Angry ipscan-3.8.2-setup.exe

* APK+Easy+Tool+v1.60+Portable.zip

* APK.Editor.Plus.v1.10.0.Dark.Mod(20210130).apk

* APK-Editor-Pro-v1.9.7-Mod.apk

* AsyncRAT v0.5.7B.zip

* CEH-v11-Study-Guide.zip

* ndp48-web.exe

* procexp64.exe

* repo.txt

* reshacker_setup.exe

* scrcpy-win64-v1.24.zip

* SpyNote Black Edition ( infinityhacks.net ).7z
